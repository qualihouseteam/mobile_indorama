import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import UserContextProvider from './src/contexts/UserContext'
import { Home, Login, Teste, MainTab, AcessoQRCode, ConfigIp } from './src/views/index';

export default () => {
  const Stack = createNativeStackNavigator();
  return (
    <UserContextProvider>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Login"
          screenOptions={{
            headerShown: false,
          }}
        >
          {/* <Stack.Screen name="Home" component={Home}
            options={{
              title: "QIoT - Seja Bem Vido",
              headerTintColor: '#333',
              headerTitleStyler: {
                fontWeight: 'bold',
                alignSelf: 'center',
              }
            }}
          /> */}
          <Stack.Screen
            name="MainTab"
            options={{ headerShown: false }} component={MainTab} />
          <Stack.Screen name="Login" options={{ headerShown: false }} component={Login} />
          <Stack.Screen name="qrcode" component={AcessoQRCode} />
          <Stack.Screen name="ConfigIp" component={ConfigIp} />
          {/* <Stack.Screen name="AreaRestrita" component={Login}/> */}
        </Stack.Navigator>
      </NavigationContainer>
    </UserContextProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

import { StyleSheet } from "react-native"

const Css = StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: '#63C2D1',
            alignItems: 'center',
            justifyContent: 'center',
        },
        tinyLogo: {
            width: 100,
            height: 100,
            marginBottom: 20,
        },
        viewLogin: {
            padding: 40,
            width: "100%",
        },
        login__button: {
            height: 60,
            backgroundColor: "#268596",
            borderRadius: 30,
            justifyContent: "center",
            alignItems: "center",
        },
        login__buttonText:{
            fontSize: 18,
            color: "#FFF",
        },


        /*     textPage: {
                 backgroundColor: 'orange',
                 padding: 20
             },
             button__home: {
                 marginRight: 40
             },
             darkbg: {
                 backgroundColor: "#333",
             },
             login__msg: {
                 fontWeight: "bold",
                 fontSize: 22,
                 color: "red",
                 marginTop: 10,
                 marginBottom: 15, 
             },
             login__form: {
                 width: "100%",
             },
             login__input: {
                 backgroundColor: "#fff",
                 fontSize: 19,
                 padding: 7,
                 marginBottom: 15,
                 width: 350,
             },
           
              login__buttonText:{
                  fontWeight: "bold",
                  fontSize: 22,
                  color: "#333",
              },
              qr__code:(display='flex')=>(
                  {
                      with: '100%',
                      height: '100%',
                      backgroundColor: '#000',
                      justifyContent: 'center',
                      display: display,
                  }
              )*/
    }
);

export { Css }
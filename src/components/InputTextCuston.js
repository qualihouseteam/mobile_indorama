import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Ionicons } from '@expo/vector-icons';
import {
    StyleSheet,
    TextInput,
    View,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
    Text,
    Platform,
    SafeAreaView,
} from 'react-native';
import styled from 'styled-components/native';
import { Css } from '../assets/css/Css';

const styles = StyleSheet.create({
    inputArea: {
        width: "100%",
        height: 60,
        backgroundColor: "#83D6E3",
        flexDirection: "row",
        borderRadius: 30,
        paddingLeft: 15,
        alignItems: "center",
        marginBottom: 15,
    },
    inputText: {
        flex: 1,
        fontSize: 16,
        color: "#268596",
        marginLeft: 10,
    },
});

// outra maneira de criar o estilo
const InputArea = styled.View`
    width: 100%;
    height: 60;
    backgroundColor: #83D6E3;
    flexDirection: row;
    borderRadius: 30;
    paddingLeft: 15;
    alignItems: center;
    marginBottom: 15;
`;

export default function InputTextCuston({ IconSvg, placeholder, editable, selectionColor, value, onChangeText, password }) {
    return (
        <View style={styles.inputArea}>
            <Ionicons name={IconSvg} size={24} color="#268596" />
            <TextInput style={styles.inputText}
                placeholder={placeholder}
                placeholderTextColor="#268596"
                value={value}
                onChangeText={onChangeText}
                secureTextEntry={password}
                selectionColor={selectionColor}
                editable={editable}
            />   
        </View>
    )
}
import React from "react";
import { Ionicons } from '@expo/vector-icons';
import {
    StyleSheet,
    TextInput,
    View,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
    Text,
    Platform,
    SafeAreaView,
} from 'react-native';

const styles = StyleSheet.create({
    inputArea: {
        height: 60,
        backgroundColor: "#83D6E3",
        alignItems: "center",
    },
    tabItem: {
        width: 70,
        height: 70,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#FFF",
        borderRadius: 35,
        borderWidth: 3,
        borderColor: "#4EADBE",
        marginTop: -20,
    },
});

export default ({ state, navigation }) => {
    const goTo = (screenName) => {
        console.log('aaaaaa@@@r', navigation);
        navigation.navigate(screenName);
    }

    return (
        <View style={styles.inputArea}>
            <TouchableOpacity style={styles.tabItem} onPress={() => goTo('Ler QR Code')}>
                <Ionicons name="aperture-sharp" size={32} color="#4EADBE" />
            </TouchableOpacity>
        </View>
    );
}
export default function filterPath(tipo, id) {
    // const url = "http://192.168.1.90/#/qrCodeLeituraMobile/id/tipo";
    //setConfirm(confirmEntrance(id, tipo));
    //setConfirm(true)

    //if(confirm){

    // url = `http://192.168.1.90/#/laudoPCPMobile/${id}`;

    let URL = '';

    switch (tipo) {
        case 'pcp':
            URL = `http://192.168.1.90/#/laudoPCPMobile/${id}`;
            break;
        case 'est':
            URL =`http://192.168.1.90/#/laudoEstiragemMobile/${id}`;
            break;
        case 'fia':
            URL =`http://192.168.1.90/#/laudoFiacoesMobile/${id}`;
            break;
        case 'anx':
            URL =`http://192.168.1.90/#/laudoAnexoMobile/${id}`;
            break;
        case 'uti':
            URL =`http://192.168.1.90/#/laudoUtilidadesMobile/${id}`;
            break;
        case 'adi':
            URL =`http://192.168.1.90/#/laudoAditivosMobile/${id}`;
            break;
        default:
            break;
    }
    return URL;
}    
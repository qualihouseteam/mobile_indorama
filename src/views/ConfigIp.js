import React, { useState, useEffect } from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    SafeAreaView,
    Keyboard,
    Alert,
    ActivityIndicator,
} from 'react-native';
import InputTextCuston from '../components/InputTextCuston';
import { useNavigation } from '@react-navigation/native';
import { Css } from '../assets/css/Css';

import AsyncStorage from '@react-native-async-storage/async-storage';

export default function ConfigIp() {

    const [ip, setIp] = useState('');

    const [passwordPass, setPasswordPass] = useState('');

    const [isLoading, setIsLoading] = useState(false);

    const navigation = useNavigation();

    const styles = {
        font: {
            fontSize: 40,
            paddingBottom: 50,
            color: '#FFF'
        },

        pading: {
            paddingTop: 10,
            justifyContent: 'center'
        }
    }

    /*    useEffect(() => {
           (async () => {
               let a;
               try {
                   if (a !== null) {
                   a = await AsyncStorage.getItem("ip");
                   }
                   a = 0
                   setIp(a);
               } catch (e) {
                   alert(e);
               }
           })();
       }); */

    const handleSetIpClick = () => {
        setIsLoading(true);
        navigation.reset({
            routes: [{ name: 'Login' }]
        });
    }

    const keepIp = async () => {
        
        setIsLoading(true);
        
        try {
            const passwordConfirm = 'indorama';
            
            console.log('aaaaaaaaaaaaaaaaa')
            
            if (passwordPass === 'indorama' && passwordPass !== null) {

                await AsyncStorage.removeItem("ip");

                await AsyncStorage.setItem("ip", ip);

                Keyboard.dismiss(); // fecha o teclado

                Alert.alert("Gravado", "IP gravado com sucesso!"); // mostra uma mensagem de alerta

                handleSetIpClick(); // redireciona o usuario para tela de login
            } else {
                Alert.alert("Erro", "Senha incorreta!")

                setIsLoading(false);
            }
        } catch (e) {
            alert(e);
        }
    }

    return (
        <SafeAreaView style={[Css.container]}>
            <View style={Css.viewLogin}>
                <Text style={styles.font}>Configurações IP</Text>
                <ActivityIndicator
                    style={styleLoading}
                    size="large"
                    color="white"
                    animating={isLoading}
                />
                <InputTextCuston
                    placeholder="Digite o IP"
                    selectionColor="white"
                    onChangeText={(ipV) => setIp(ipV)}
                    value={ip}
                    editable={!isLoading}
                />
                <InputTextCuston
                    placeholder="Digite senha de segurança"
                    selectionColor="white"
                    value={passwordPass}
                    onChangeText={(v) => setPasswordPass(v)}
                    password={true}
                    editable={!isLoading}
                />
                <TouchableOpacity disabled={isLoading} style={Css.login__button}>
                    <Text onPress={keepIp} style={Css.login__buttonText}>Gravar IP</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={handleSetIpClick} style={styles.pading} >
                <Text style={{color: 'white'}}>Volte para a tela de Acesso</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}
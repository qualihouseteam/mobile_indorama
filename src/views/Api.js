import AsyncStorage from '@react-native-async-storage/async-storage';

/* async function valueIP() {
    try{
        const res = await AsyncStorage.getItem("ip");
        return res;
    }catch(e) {
        alert(e)
    }
 
} */

//const b = AsyncStorage.getItem('ip');
/* console.log('sdasdsadsadas', valueIP)
let BASE_API = `http://${valueIP}/api_doka_pack/v1`; */

export default {

    login: async (loginUser, password) => {

        let res = 0;

        try{
            res = await AsyncStorage.getItem("ip");

            if(res === null){
                res = ''
            }
        }catch(e) {
            alert(e)
        }

        let BASE_API = `http://${res}/api_doka_pack/v1`;

        let customer = {
            loginUser,
            password,
        };
        let bodyValue = {
          customer,
        };
        bodyValue = JSON.stringify(bodyValue);
        const req = await fetch(`${BASE_API}/login`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: bodyValue,
        });
        //const json = await req.json();
        return req.json();
    },

    logout: async () => {

    },

}




/*import axios from "axios";

const Api = axios.create(
    {
        baseURL: 'http://192.168.1.123/api_doka_pack/v1',
    },
);

export default Api;*/
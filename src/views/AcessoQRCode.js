import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { WebView } from 'react-native-webview';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function AcessoQRCode() {

    const navigation = useNavigation();
    const [tipo, setTipo] = useState('');
    const [id, setId] = useState('');
    const [ip, setIp] = useState('');
    const [idUser, setIdUser] = useState('');

    useEffect(() => {
        (async () => {
            setIdUser(await AsyncStorage.getItem("idUser"));
            setIp(await AsyncStorage.getItem("ip"));
            setId(await AsyncStorage.getItem("idLaudo"));
            setTipo(await AsyncStorage.getItem("tipo"));
        })();
    }, []);

    const goTo = async () => {
        await AsyncStorage.setItem('idUser', '');
        navigation.reset({
            routes: [{ name: 'Login' }]
        });
    }

    const openSistemaQrCode = () => {
        const URL = `http://${ip}/#/qrCodeLeituraMobile/${id}/${tipo}/${idUser}`;
        console.log('url', URL);
        if (id !== '' && id !== null && id !== undefined) {
            return (
                <>
                    <Button
                        onPress={() => goTo()}
                        title="Sair"
                        color="#63C2D1"
                        accessibilityLabel="Learn more about this purple button"
                   />
                    <WebView source={{ uri: URL }}>
                    </WebView >
                </>
            );
        }
    }

    return (
        <>
            {openSistemaQrCode()}
        </>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
});
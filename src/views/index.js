import Home from './Home';
import Login from './Login';
import MainTab from './MainTab';
import AcessoQRCode from './AcessoQRCode';
import ConfigIp from './ConfigIp';

export { Home, Login, MainTab, AcessoQRCode, ConfigIp };
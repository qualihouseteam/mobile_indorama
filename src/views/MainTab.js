import React from 'react';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import CustomTabBar from '../components/CustomTabBar';
import Home from './Home';
import Login from './Login';
import AcessoQRCode from './AcessoQRCode';

const Tab = createBottomTabNavigator();

export default () => (
    <>
        <Tab.Navigator tabBar={props => <CustomTabBar {...props} />}>
            <Tab.Screen name="Ler QR Code" component={Home} />
            <Tab.Screen name="Laudo" component={AcessoQRCode} />
        </Tab.Navigator>
        <Tab.Screen name="Login" component={Login} />
    </>
)
import React, { useState, useContext } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
    Text,
    Platform,
    SafeAreaView,
    Button,
    ActivityIndicator,
    Alert,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import InputTextCuston from '../components/InputTextCuston';
import Api from './Api';
import { UserContext } from '../contexts/UserContext';
import { useNavigation } from '@react-navigation/native';
import { Css } from '../assets/css/Css';

export default function Login() {
    const { dispatch: userDispatch } = useContext(UserContext);
    const navigation = useNavigation();
    const [emailFild, setEmailFild] = useState('');
    const [passeordlFild, setPasswordFild] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const hadleLoginClick = async () => {
        setIsLoading(true);
        if (emailFild != '' && passeordlFild != '') {
            let json = await Api.login(emailFild, passeordlFild);
            console.log('json', json);
            if (json.status == "success") {
                await AsyncStorage.setItem("idUser", json.uid);
                userDispatch({
                    type: 'id',
                    payload: {
                        id: json.data,
                    },
                });
                navigation.reset({
                    routes: [{ name: 'MainTab' }]
                });
            } else {
                setIsLoading(false);
                Alert.alert("Erro","Email ou senha incorretos!");
            }
        } else {
            console.log('alertaaaa');
            setIsLoading(false);
            Alert.alert("Erro","Preencha os campos!");
        }
    }

    const handleSetIpClick = () => {
        setIsLoading(true)
        navigation.reset({
            routes: [{ name: 'ConfigIp' }]
        });
    }
    
    styleLoading = {
        height: 30,
        weight: 30,
        paddingBottom: 30,
        paddingTop: -50,
    }

    return (
        <SafeAreaView style={[Css.container]}>
            <Image source={require('../assets/img/icon.png')} style={Css.tinyLogo} />
            <View style={Css.viewLogin}>
                <ActivityIndicator
                    style={styleLoading}
                    size="large"
                    color="white"
                    animating={isLoading}
                />
                <InputTextCuston
                    IconSvg="mail"
                    placeholder="Digite seu login."
                    value={emailFild}
                    onChangeText={t => setEmailFild(t)}
                    selectionColor="white"
                    editable={!isLoading}
                />
                <InputTextCuston
                    IconSvg="lock-closed"
                    placeholder="Digite sua senha."
                    value={passeordlFild}
                    onChangeText={t => setPasswordFild(t)}
                    password={true}
                    selectionColor="white"
                    editable={!isLoading}
                />
                {/*  {isLoading &&
                    <ActivityIndicator
                        size="large"
                        color="white"
                        animating={isLoading}
                    />
                }
                {!isLoading &&
                    <TouchableOpacity style={Css.login__button} onPress={hadleLoginClick}>
                        <Text style={Css.login__buttonText}>Entrar</Text>
                    </TouchableOpacity>
                } */}
                <TouchableOpacity disabled={isLoading} style={Css.login__button} onPress={hadleLoginClick}>
                    <Text style={Css.login__buttonText}>Entrar</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity disabled={isLoading}>
                <Text style={{ color: 'white' }} onPress={handleSetIpClick}>Clique aqui para alterar o IP</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
    // }
}

/*
 <KeyboardAvoidingView style={[Css.container, Css.darkbg]} behavior={Platform.OS === "ios" ? "padding" : "height"}>
            <View>
                <Image source={require('../assets/img/icon.png')} />
            </View>
            <View>
                <Text style={Css.login__msg}>Usuário ou senha inválido</Text>
            </View>
            <View>
                <TextInput placeholder='Usuário' style={Css.login__input}></TextInput>
                <TextInput placeholder='Senha' secureTextEntry={true} style={Css.login__input}></TextInput>
                <TouchableOpacity style={Css.login__button}>
                    <Text style={Css.login__buttonText}>Entrar</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
        */

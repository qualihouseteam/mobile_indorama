import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { WebView } from 'react-native-webview';
import { useNavigation } from '@react-navigation/native';
import confirmEntrance from './confirmEntrance';
import AsyncStorage from '@react-native-async-storage/async-storage';

import AcessoQRCode from './AcessoQRCode';


export default function Home() {

    const navigation = useNavigation();
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [qrCode, setQrCode] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const handleBarCodeScannedd = async ({ type, data }) => {
        setScanned(true);
        console.log('data - ', data)
        const answer_array = data.split(':');
        console.log('tipo', answer_array[0])
        console.log('id', answer_array[1])

        await AsyncStorage.setItem("tipo", answer_array[0]);
        await AsyncStorage.setItem("idLaudo", answer_array[1]);
        setQrCode(true);
    };

    if (hasPermission === null) {
        return <Text>Requisitando Acesso a Camera</Text>;
    }
    if (hasPermission === false) {
        return <Text>Sem Acesso a Camera</Text>;
    }

    const goTo = async () => {
        await AsyncStorage.setItem('idUser', '');
        navigation.reset({
            routes: [{ name: 'Login' }]
        });
    }

    function openCam() {
        return (
            <>
                 <Button
                    onPress={() => goTo()}
                    title="Sair"
                    color="#63C2D1"
                    accessibilityLabel="Learn more about this purple button"
                />
                <View style={styles.container}>
                    <BarCodeScanner
                        onBarCodeScanned={scanned ? undefined : handleBarCodeScannedd}
                        style={StyleSheet.absoluteFillObject}
                    />
                    {scanned && <Button title={'Click para ler novamente!'} onPress={() => setScanned(false)} />}
                </View>
            </>
        );
    }



    const openSistemaQrCode = () => {

        console.log('aaa')

        navigation.reset({
            routes: [{ name: 'Laudo' }]
        });

    }


    return (
        <>
            {
                qrCode ? (
                    openSistemaQrCode()
                ) : openCam()
            }
        </>

    );
    /* const url = "http://sys.qiot.com.br";
     return (
 
         <WebView source={{ uri: url }}>
         </WebView >
     ); */

}

/* async function confirm(tipo, id) {
    const res = await confirmEntrance(tipo, id);
    console.log('res do confirm', res);
    if (res !== 0) {
        return true;
    } else {
        return false;
    }
}
 */
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
});